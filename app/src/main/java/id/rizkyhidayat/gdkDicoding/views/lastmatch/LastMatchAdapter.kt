package id.rizkyhidayat.gdkDicoding.views.lastmatch

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import id.rizkyhidayat.gdkDicoding.R
import id.rizkyhidayat.gdkDicoding.models.Event
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_list_last_match.*

/**
 * @author <M.Rizky Hidayat, emrizkyha@gmail.com>
 */
class LastMatchAdapter(
        private val lastMatch: List<Event>,
        var listener: (Event) -> Unit
) : RecyclerView.Adapter<LastMatchAdapter.LastMatchHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LastMatchHolder {
        return LastMatchHolder(
                LayoutInflater.from(parent.context).inflate(R.layout.item_list_last_match, parent, false)
        )
    }

    override fun getItemCount(): Int = lastMatch.size

    override fun onBindViewHolder(holder: LastMatchHolder, position: Int) {
        holder.bindItem(lastMatch[position], listener)
    }

    inner class LastMatchHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView),
            LayoutContainer {

        fun bindItem(lastMatch: Event, listener: (Event) -> Unit) {
            dateMatch.text = lastMatch.dateEvent
            homeMatch.text = lastMatch.strHomeTeam
            awayMatch.text = lastMatch.strAwayTeam
            homeScoreMatch.text = lastMatch.strHomeScore ?: " "
            awayScoreMatch.text = lastMatch.strAwayScore ?: " "

            containerView.setOnClickListener {
                listener(lastMatch)
            }
        }
    }
}