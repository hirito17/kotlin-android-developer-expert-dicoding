package id.rizkyhidayat.gdkDicoding.presenter.nextmatch

import android.annotation.SuppressLint
import id.rizkyhidayat.gdkDicoding.services.ApiServices
import id.rizkyhidayat.gdkDicoding.utils.CoroutineContextProvider
import id.rizkyhidayat.gdkDicoding.utils.InternetChecker
import id.rizkyhidayat.gdkDicoding.utils.Network
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

/**
 * @author <M.Rizky Hidayat, emrizkyha@gmail.com>
 */
class NextMatchPresenter(
        private val network: Network,
        override var view: NextMatchContracts.View,
        private val contextPool: CoroutineContextProvider = CoroutineContextProvider()
) : NextMatchContracts.Presenter {


    override fun start() {

    }

    @SuppressLint("CheckResult")
    override fun getNextMatch() {
        GlobalScope.launch(contextPool.main) {
            view.showLoading()
            InternetChecker(object : InternetChecker.Consumer {
                override fun accept(internet: Boolean) {
                    if (internet) {
                        network.getInstance()
                                .create(ApiServices::class.java)
                                .eventNextLeague()
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe({
                                    view.hideLoading()
                                    view.showEventList(it.events)
                                }, {
                                    error(it)
                                })
                    } else {
                        view.hideLoading()
                        view.showSnackBar("You don't connect with internet")
                    }
                }
            })
        }
    }

}