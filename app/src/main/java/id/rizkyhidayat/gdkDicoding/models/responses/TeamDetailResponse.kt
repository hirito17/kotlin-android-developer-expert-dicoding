package id.rizkyhidayat.gdkDicoding.models.responses

import id.rizkyhidayat.gdkDicoding.models.TeamDetail

/**
 * @author <M.Rizky Hidayat, emrizkyha@gmail.com>
 */
data class TeamDetailResponse(
        var teams: List<TeamDetail>
)