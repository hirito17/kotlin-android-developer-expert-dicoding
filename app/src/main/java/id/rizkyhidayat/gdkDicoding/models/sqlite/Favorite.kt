package id.rizkyhidayat.gdkDicoding.models.sqlite

/**
 * @author <M.Rizky Hidayat, emrizkyha@gmail.com>
 */
data class Favorite(
        var id: Long? = null,

        var eventId: String? = null,

        var idHomeTeam: String? = null,

        var idAwayTeam: String? = null,

        var strHomeTeam: String? = null,

        var strAwayTeam: String? = null,

        var strHomeScore: String? = null,

        var strAwayScore: String? = null,

        var dateEvent: String? = null
) {

    companion object {
        const val TABLE_NAME: String = "FAVORITE"
        const val ID: String = "ID_"
        const val EVENT_ID: String = "EVENT_ID"
        const val HOME_TEAM_ID: String = "HOME_TEAM_ID"
        const val AWAY_TEAM_ID: String = "AWAY_TEAM_ID"
        const val HOME_TEAM_NAME: String = "HOME_TEAM_NAME"
        const val AWAY_TEAM_NAME: String = "AWAY_TEAM_NAME"
        const val HOME_TEAM_SCORE: String = "HOME_TEAM_SCORE"
        const val AWAY_TEAM_SCORE: String = "AWAY_TEAM_SCORE"
        const val DATE_EVENT: String = "DATE_EVENt"
    }

}