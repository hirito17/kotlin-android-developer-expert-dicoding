package id.rizkyhidayat.gdkDicoding.views.favorite

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import id.rizkyhidayat.gdkDicoding.R
import id.rizkyhidayat.gdkDicoding.models.Event
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_list_favorite.*

/**
 * @author <M.Rizky Hidayat, emrizkyha@gmail.com>
 */
class FavoriteAdapter(
        private val items: List<Event>,
        var listener: (Event) -> Unit
) : RecyclerView.Adapter<FavoriteAdapter.FavoriteHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FavoriteHolder {
        return FavoriteHolder(
                LayoutInflater.from(parent.context).inflate(R.layout.item_list_favorite, parent, false)
        )
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: FavoriteHolder, position: Int) {
        holder.bindItem(items[position], listener)
    }

    inner class FavoriteHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView),
            LayoutContainer {

        fun bindItem(item: Event, listener: (Event) -> Unit) {
            dateMatch.text = item.dateEvent
            homeMatch.text = item.strHomeTeam
            awayMatch.text = item.strAwayTeam
            homeScoreMatch.text = item.strHomeScore ?: " "
            awayScoreMatch.text = item.strAwayScore ?: " "

            containerView.setOnClickListener {
                listener(item)
            }
        }

    }
}