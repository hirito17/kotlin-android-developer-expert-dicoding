package id.rizkyhidayat.gdkDicoding.presenter

/**
 * @author <M.Rizky Hidayat, emrizkyha@gmail.com>
 */

interface BaseView<out T : BasePresenter<*>> {

    val presenter: T

}