package id.rizkyhidayat.gdkDicoding.views.matchdetail

import android.database.sqlite.SQLiteConstraintException
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import com.bumptech.glide.Glide
import id.rizkyhidayat.gdkDicoding.R
import id.rizkyhidayat.gdkDicoding.R.id.addToFavorite
import id.rizkyhidayat.gdkDicoding.database.database
import id.rizkyhidayat.gdkDicoding.models.Event
import id.rizkyhidayat.gdkDicoding.models.EventDetail
import id.rizkyhidayat.gdkDicoding.models.TeamDetail
import id.rizkyhidayat.gdkDicoding.models.sqlite.Favorite
import id.rizkyhidayat.gdkDicoding.presenter.matchdetail.MatchDetailContracts
import id.rizkyhidayat.gdkDicoding.presenter.matchdetail.MatchDetailPresenter
import id.rizkyhidayat.gdkDicoding.utils.Network
import id.rizkyhidayat.gdkDicoding.utils.invisible
import id.rizkyhidayat.gdkDicoding.utils.showSnackbar
import id.rizkyhidayat.gdkDicoding.utils.visible
import kotlinx.android.synthetic.main.activity_detail_match.*
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.delete
import org.jetbrains.anko.db.insert
import org.jetbrains.anko.db.select
import org.jetbrains.anko.support.v4.onRefresh

/**
 * @author <M.Rizky Hidayat, emrizkyha@gmail.com>
 */
class MatchDetailActivity : AppCompatActivity(), MatchDetailContracts.View {

    companion object {
        var PARCEL = "parcel"
    }

    override lateinit var presenter : MatchDetailPresenter
    private var isFavorite: Boolean = false
    lateinit var event : Event
    lateinit var eventId : String
    private var menuItem: Menu? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_match)

        supportActionBar?.title = "Match Detail"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        event = intent.getParcelableExtra(PARCEL)
        eventId = event.idEvent.toString()

        favoriteState()
        presenter = MatchDetailPresenter(Network(), this)
        presenter.getMatchDetail(event.idEvent.toString())
        presenter.getHomeTeamDetail(event.idHomeTeam.toString())
        presenter.getAwayTeamDetail(event.idAwayTeam.toString())
        swipeRefreshEventDetail.isRefreshing = false

        swipeRefreshEventDetail.onRefresh {
            presenter.getMatchDetail(event.idEvent.toString())
            presenter.getHomeTeamDetail(event.idHomeTeam.toString())
            presenter.getAwayTeamDetail(event.idAwayTeam.toString())
            swipeRefreshEventDetail.isRefreshing = false
        }
    }

    private fun favoriteState() {
        database.use{
            val result = select(Favorite.TABLE_NAME)
                    .whereArgs("(EVENT_ID = {id})", "id" to eventId)
            val favorite = result.parseList(classParser<Favorite>())
            if (!favorite.isEmpty())
                isFavorite = true
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.detail_menu, menu)
        menuItem = menu
        setFavorite()
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            addToFavorite -> {
                if (isFavorite)
                    removeFromFavoriteDb()
                else
                    addToFavoriteDb()

                isFavorite = !isFavorite

                setFavorite()
                true
            }

            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun addToFavoriteDb() {
        try {
            database.use {
                insert(Favorite.TABLE_NAME,
                        Favorite.EVENT_ID to event.idEvent,
                        Favorite.HOME_TEAM_ID to event.idHomeTeam,
                        Favorite.AWAY_TEAM_ID to event.idAwayTeam,
                        Favorite.HOME_TEAM_NAME to event.strHomeTeam,
                        Favorite.AWAY_TEAM_NAME to event.strAwayTeam,
                        Favorite.HOME_TEAM_SCORE to event.strHomeScore,
                        Favorite.AWAY_TEAM_SCORE to event.strAwayScore,
                        Favorite.DATE_EVENT to event.dateEvent)
            }
            swipeRefreshEventDetail.showSnackbar(getString(R.string.success_favorite), Snackbar.LENGTH_SHORT)
        } catch (e: SQLiteConstraintException) {
            swipeRefreshEventDetail.showSnackbar(e.localizedMessage, Snackbar.LENGTH_SHORT)
        }
    }

    private fun removeFromFavoriteDb() {
        try {
            database.use {
                delete(Favorite.TABLE_NAME,
                        "(EVENT_ID = {ID})",
                        "ID" to eventId)
            }
            swipeRefreshEventDetail.showSnackbar(getString(R.string.remove_favorite), Snackbar.LENGTH_SHORT)
        } catch (e: SQLiteConstraintException) {
            swipeRefreshEventDetail.showSnackbar(e.localizedMessage, Snackbar.LENGTH_SHORT)
        }
    }

    private fun setFavorite() {
        if (isFavorite)
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, R.drawable.ic_has_added_to_favorites)
        else
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, R.drawable.ic_add_to_favorites)
    }

    override fun showMatchDetail(data: List<EventDetail>) {
        dateEventDetail.text = data[0].dateEvent ?: ""
        homeScoreEventDetail.text = data[0].intHomeScore?.toString() ?: ""
        awayScoreEventDetail.text = data[0].intAwayScore?.toString() ?: ""
        homeGoalsEventDetail.text = data[0].strHomeGoalDetails?.replace(';', '\n') ?: ""
        awayGoalsEventDetail.text = data[0].strAwayGoalDetails?.replace(';', '\n') ?: ""
        homeShotsEventDetail.text = data[0].intHomeShots?.toString() ?: ""
        awayShotsEventDetail.text = data[0].intAwayShots?.toString() ?: ""
        homeGoalKeeperEventDetail.text = data[0].strHomeLineupGoalkeeper?.replace(';', '\n') ?: ""
        awayGoalKeeperEventDetail.text = data[0].strAwayLineupGoalkeeper?.replace(';', '\n') ?: ""
        homeDefenseEventDetail.text = data[0].strHomeLineupDefense?.replace(';', '\n') ?: ""
        awayDefenseEventDetail.text = data[0].strAwayLineupDefense?.replace(';', '\n') ?: ""
        homeMidFieldEventDetail.text = data[0].strHomeLineupMidfield?.replace(';', '\n') ?: ""
        awayMidFieldEventDetail.text = data[0].strAwayLineupMidfield?.replace(';', '\n') ?: ""
        homeForwardEventDetail.text = data[0].strHomeLineupForward?.replace(';', '\n') ?: ""
        awayForwardEventDetail.text = data[0].strAwayLineupForward?.replace(';', '\n') ?: ""
        homeSubtitutesEventDetail.text = data[0].strHomeLineupSubstitutes?.replace(';', '\n') ?: ""
        awaySubtitutesEventDetail.text = data[0].strAwayLineupSubstitutes?.replace(';', '\n') ?: ""
    }

    override fun showHomeTeamDetail(data: List<TeamDetail>) {
        Glide.with(this@MatchDetailActivity).load(data[0].strTeamBadge).into(homeBadgeEventDetail)
    }

    override fun showAwayTeamDetail(data: List<TeamDetail>) {
        Glide.with(this@MatchDetailActivity).load(data[0].strTeamBadge).into(awayBadgeEventDetail)
    }

    override fun showLoading() {
        proggressBarEventDetail.visible()
    }

    override fun hideLoading() {
        proggressBarEventDetail.invisible()
    }

    override fun showSnackBar(message: String) {
        swipeRefreshEventDetail.showSnackbar(message, Snackbar.LENGTH_SHORT)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()

        return super.onSupportNavigateUp()
    }
}