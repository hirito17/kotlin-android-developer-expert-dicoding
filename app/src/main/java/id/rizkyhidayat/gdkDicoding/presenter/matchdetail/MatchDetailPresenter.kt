package id.rizkyhidayat.gdkDicoding.presenter.matchdetail

import android.annotation.SuppressLint
import id.rizkyhidayat.gdkDicoding.services.ApiServices
import id.rizkyhidayat.gdkDicoding.utils.CoroutineContextProvider
import id.rizkyhidayat.gdkDicoding.utils.InternetChecker
import id.rizkyhidayat.gdkDicoding.utils.Network
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

/**
 * @author <M.Rizky Hidayat, emrizkyha@gmail.com>
 */
class MatchDetailPresenter(
        private val network: Network,
        override var view: MatchDetailContracts.View,
        private val contextPool: CoroutineContextProvider = CoroutineContextProvider()
) : MatchDetailContracts.Presenter {

    override fun start() {}

    @SuppressLint("CheckResult")
    override fun getMatchDetail(idMatch: String) {
        GlobalScope.launch(contextPool.main) {
            view.showLoading()
            InternetChecker(object : InternetChecker.Consumer {
                override fun accept(internet: Boolean) {
                    if (internet) {
                        GlobalScope.launch(contextPool.main) {
                            network.getInstance().create(ApiServices::class.java)
                                    .eventDetail(idMatch)
                                    .subscribeOn(Schedulers.io())
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribe({
                                        view.hideLoading()
                                        view.showMatchDetail(it.events)
                                    }, {
                                        error(it)
                                    })
                        }
                    } else {
                        view.hideLoading()
                        view.showSnackBar("You don't connect with internet")
                    }
                }
            })
        }
    }

    @SuppressLint("CheckResult")
    override fun getHomeTeamDetail(idTeam: String) {
        GlobalScope.launch(contextPool.main) {
            view.showLoading()
            InternetChecker(object : InternetChecker.Consumer {
                override fun accept(internet: Boolean) {
                    if (internet) {
                        network.getInstance().create(ApiServices::class.java)
                                .teamDetail(idTeam)
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe({
                                    view.hideLoading()
                                    view.showHomeTeamDetail(it.teams)
                                }, {
                                    error(it)
                                })
                    } else {
                        view.hideLoading()
                        view.showSnackBar("You don't connect with internet")
                    }
                }
            })
        }
    }

    @SuppressLint("CheckResult")
    override fun getAwayTeamDetail(idTeam: String) {
        GlobalScope.launch(contextPool.main) {
            view.showLoading()
            InternetChecker(object : InternetChecker.Consumer {
                override fun accept(internet: Boolean) {
                    if (internet) {
                        network.getInstance().create(ApiServices::class.java)
                                .teamDetail(idTeam)
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe({
                                    view.hideLoading()
                                    view.showAwayTeamDetail(it.teams)
                                }, {
                                    error(it)
                                })
                    } else {
                        view.hideLoading()
                        view.showSnackBar("You don't connect with internet")
                    }
                }
            })
        }
    }

}