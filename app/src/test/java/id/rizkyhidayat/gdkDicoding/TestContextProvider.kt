package id.rizkyhidayat.gdkDicoding

import id.rizkyhidayat.gdkDicoding.utils.CoroutineContextProvider
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlin.coroutines.CoroutineContext

/**
 * @author <M.Rizky Hidayat, emrizkyha@gmail.com>
 */
class TestContextProvider : CoroutineContextProvider() {
    @ExperimentalCoroutinesApi
    override val main: CoroutineContext = Dispatchers.Unconfined
}